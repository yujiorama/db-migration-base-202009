#!/bin/bash

set -o pipefail
set -eux

git clone --depth 1 https://github.com/jOOQ/jOOQ /tmp/jooq
pushd /tmp/jooq/jOOQ-examples/Sakila || exit

if command -v mysql; then
    pushd mysql-sakila-db || exit
    ls -l
    set +e
    mysql --user="root" --password="${MYSQL_ROOT_PASSWORD}" -B --silent sakila < mysql-sakila-delete-data.sql
    mysql --user="root" --password="${MYSQL_ROOT_PASSWORD}" -B --silent sakila < mysql-sakila-drop-objects.sql
    set -e
    mysql --user="root" --password="${MYSQL_ROOT_PASSWORD}" -B --silent sakila < mysql-sakila-schema.sql
    mysql --user="root" --password="${MYSQL_ROOT_PASSWORD}" -B --silent sakila < mysql-sakila-insert-data.sql
fi

if command -v psql; then
    pushd postgres-sakila-db || exit
    ls -l
    set +e
    PASSWORD="${POSTGRES_PASSWORD}" psql --username="${POSTGRES_USER}" -b -q sakila < postgres-sakila-delete-data.sql
    PASSWORD="${POSTGRES_PASSWORD}" psql --username="${POSTGRES_USER}" -b -q sakila < postgres-sakila-drop-objects.sql
    set -e
    PASSWORD="${POSTGRES_PASSWORD}" psql --username="${POSTGRES_USER}" -b -q sakila < postgres-sakila-schema.sql
    PASSWORD="${POSTGRES_PASSWORD}" psql --username="${POSTGRES_USER}" -b -q sakila < postgres-sakila-insert-data.sql
fi

set +eux
